import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:nawlinga/Blocs/Signup/signup_bloc.dart';
import 'package:nawlinga/Models/Reservation/Reservation.dart';
import 'package:nawlinga/Models/User/User.dart';
import 'package:nawlinga/Utility/Firebase/FirebaseServices.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  @override
  get initialState => InitialState();

  @override
  Stream<HomeState> mapEventToState(event) async* {
    if (event is RetrieveHomeUserInfo) {
      yield await retrieveUserInformation(event);
    }
     else if (event is RetrieveHomeUserReservations) {
      yield await retrieveReservations(event);
    }
  }
}

Future<HomeState> retrieveUserInformation(HomeEvent event) async {
  FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();

  if (currentUser != null) {
    DataSnapshot snapShot = await FirebaseReferences.userAccountsReference
        .child(currentUser.uid)
        .once();

    Map snapShotMap = new Map<String, dynamic>.from(snapShot.value);
    User user = User.fromJson(snapShotMap);

    if (user == null) {
      return UserAccountInformation(
          status: ValidationStatus.firebaseError,
          message: "An error occurred while retrieving user information");
    } else {
      return UserAccountInformation(
          status: ValidationStatus.success,
          message: "User Account information was retrieved successfully",
          user: user);
    }
  } else {
    print(
        "No user account was found. Sign out and navigate to the /welcome page");
    return UserAccountInformation(
        status: ValidationStatus.firebaseError, message: "No user account");
  }
}

Future<HomeState> retrieveReservations(HomeEvent event) async {
  List<Reservation> reservations = [];
  FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();

  if (currentUser != null) {
    DataSnapshot snapShot = await FirebaseReferences
        .userAccountReservationsReference
        .child(currentUser.uid)
        .limitToLast(10)
        .once();

    Map<dynamic, dynamic> values = snapShot.value;

    if (values != null) {
      values.forEach((key, value) {
        Map snapShotMap = new Map<String, dynamic>.from(value);
        Reservation reservation = Reservation.fromJson(snapShotMap);

        reservations.add(reservation);
      });

      return UserReservations(
          status: ValidationStatus.success,
          message: "An error occurred while retrieving user information",
          listOfReservations: reservations);
    }
    else {
      return UserReservations(
          status: ValidationStatus.firebaseError,
          message: "No reservations were found for this user");
    }
  } else {
    print(
        "No user account was found. Sign out and navigate to the /welcome page");
    return UserReservations(
        status: ValidationStatus.firebaseError, message: "No user account");
  }
}
