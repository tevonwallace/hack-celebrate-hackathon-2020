
part of 'home_bloc.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class RetrieveHomeUserInfo extends HomeEvent {
  RetrieveHomeUserInfo();
  @override
  List<Object> get props => [];
}


class RetrieveHomeUserReservations extends HomeEvent {
  RetrieveHomeUserReservations();
  @override
  List<Object> get props => [];
}
