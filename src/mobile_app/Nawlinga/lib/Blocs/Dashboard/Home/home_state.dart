part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class InitialState extends HomeState {
  @override
  List<Object> get props => [];
}

class UserAccountInformation extends HomeState {
  final ValidationStatus status;
  final String message;
  final User user;
  
  UserAccountInformation({this.status, this.message, this.user});

  @override
  List<Object> get props => [this.status, this.user];
}

class UserReservations extends HomeState {
  final ValidationStatus status;
  final String message;
  final List<Reservation> listOfReservations;
  
  UserReservations({this.status, this.message, this.listOfReservations});

  @override
  List<Object> get props => [this.status, this.message, this.listOfReservations];
}

