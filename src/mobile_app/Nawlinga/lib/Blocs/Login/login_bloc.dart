import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:nawlinga/Blocs/Signup/signup_bloc.dart';
import 'package:nawlinga/Models/User/User.dart';
import 'package:nawlinga/Utility/DataManipulation.dart';
import 'package:nawlinga/Utility/Firebase/FirebaseServices.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  @override
  get initialState => InitialState();

  @override
  Stream<LoginState> mapEventToState(event) async* {
    if (event is AppleAuthenticate) {
      validateAppleLogin(event);
    } else if (event is GoogleAuthenticate) {
      yield await validateGoogleLogin(event);
    } else if (event is FacebookAuthenticate) {
    } else if (event is EmailAndPasswordAuthenticate) {
      yield await validateEmailAndPasswordLoginForm(event);
    } else if (event is LoginResetStateEvent) {
      yield initialState;
    }
  }
}

LoginState validateAppleLogin(LoginEvent event) {
// Apple Sign in logic here

  return null;
}

Future<LoginState> validateEmailAndPasswordLoginForm(
    EmailAndPasswordAuthenticate event) async {
  if ((event.email.isEmpty) || (!DataManipulation.isEmailValid(event.email))) {
    return FirebaseAuthenticationForEmailAndPasswordState(
        status: ValidationStatus.formError,
        message: "Please enter a valid email address.");
  } else if (event.password.isEmpty) {
    return FirebaseAuthenticationForEmailAndPasswordState(
        status: ValidationStatus.formError,
        message: "Please enter a valid password.");
  } else if (event.password.length < 8) {
    return FirebaseAuthenticationForEmailAndPasswordState(
        status: ValidationStatus.formError,
        message: "Your password must be at least 8 characters.");
  } else {
    return await attemptToSignInUserWithEmailAndPassword(event);
  }
}

Future<LoginState> attemptToSignInUserWithEmailAndPassword(
    EmailAndPasswordAuthenticate event) async {
  FirebaseAuthenticationForEmailAndPasswordState signUpStatusState;

  try {
    AuthResult value = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: event.email, password: event.password);

    if (value.user != null) {
      signUpStatusState = FirebaseAuthenticationForEmailAndPasswordState(
          status: ValidationStatus.success, message: "Sign in was successful.");
      return signUpStatusState;
    } else {
      return FirebaseAuthenticationForEmailAndPasswordState(
          status: ValidationStatus.firebaseError,
          message: "An error occured when signing in with email and password.");
    }
  } on PlatformException catch (exception) {
    signUpStatusState = FirebaseAuthenticationForEmailAndPasswordState(
        status: ValidationStatus.firebaseError, message: exception.message);
  }

  return signUpStatusState;
}

Future<LoginState> validateGoogleLogin(LoginEvent event) async {
  FirebaseUser user = await FirebaseServices.getInstance().getCurrentUser();

  if (user != null) {
    return FirebaseAuthenticationState(user);
  } else {
    user = await _handleSignIn();
  }

  return FirebaseAuthenticationState(user);
}

Future<FirebaseUser> _handleSignIn() async {
  try {
    final GoogleSignIn _googleSignIn = GoogleSignIn();
    final FirebaseAuth _auth = FirebaseAuth.instance;

    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final FirebaseUser user =
        (await _auth.signInWithCredential(credential)).user;

    if (user != null) {
      String displayName = googleUser.displayName;
      List<String> displayNameList = displayName.split(" ");

      User newUser = new User(
          userId: user.uid,
          firstName: displayNameList[0],
          lastName: (displayNameList.length > 1 ? displayNameList[1] : ""),
          email: googleUser.email.trim());

      FirebaseReferences.userAccountsReference
          .child(user.uid)
          .set(newUser.toJson());
    }

    return user;
  } on PlatformException {
    return null;
  }
}

LoginState validateFacebookLogin(LoginEvent event) {
// Facebook Sign in logic here

  return null;
}
