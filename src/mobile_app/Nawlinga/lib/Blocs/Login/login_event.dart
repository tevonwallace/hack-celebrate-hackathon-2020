part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class LoginResetStateEvent extends LoginEvent {
  LoginResetStateEvent();
  @override
  List<Object> get props => [];
}

class AppleAuthenticate extends LoginEvent {
  final String username;
  final String password;

  AppleAuthenticate(this.username, this.password);
  @override
  List<Object> get props => [password, username];
}

class EmailAndPasswordAuthenticate extends LoginEvent {
  final String email;
  final String password;

  EmailAndPasswordAuthenticate({this.email, this.password});
  @override
  List<Object> get props => [this.email, this.password];
}

class GoogleAuthenticate extends LoginEvent {
  GoogleAuthenticate();
  @override
  List<Object> get props => [];
}

class FacebookAuthenticate extends LoginEvent {
  final String username;
  final String password;

  FacebookAuthenticate(this.username, this.password);
  @override
  List<Object> get props => [password, username];
}
