part of 'login_bloc.dart';

abstract class LoginState extends Equatable {
  const LoginState();
}

class InitialState extends LoginState {
  @override
  List<Object> get props => [];
}

class FirebaseAuthenticationForEmailAndPasswordState extends LoginState {
  final ValidationStatus status;
  final String message;
  
  FirebaseAuthenticationForEmailAndPasswordState({this.status, this.message});

  @override
  List<Object> get props => [this.status, this.message];
}

class FirebaseAuthenticationState extends LoginState {
  final FirebaseUser user;

  FirebaseAuthenticationState(this.user);

  @override
  List<Object> get props => [this.user];
}

class ValidatationState extends LoginState {
  final String validateMsg;
  final bool success;

  ValidatationState(this.validateMsg, this.success);

  @override
  List<Object> get props => [validateMsg];
}
