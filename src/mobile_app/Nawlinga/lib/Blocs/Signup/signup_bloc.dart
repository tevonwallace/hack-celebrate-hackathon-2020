import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nawlinga/Models/User/User.dart';
import 'package:nawlinga/Utility/DataManipulation.dart';
import 'package:nawlinga/Utility/Firebase/FirebaseServices.dart';
import 'package:string_validator/string_validator.dart';

part 'signup_event.dart';
part 'signup_state.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  @override
  get initialState => InitialState();

  final TextEditingController firstname = new TextEditingController();
  final TextEditingController lastname = new TextEditingController();
  final TextEditingController email = new TextEditingController();
  final TextEditingController pwd = new TextEditingController();
  final TextEditingController pwdConfirmation = new TextEditingController();
  final TextEditingController trn = new TextEditingController();

  @override
  Stream<SignUpState> mapEventToState(event) async* {
    if (event is SignUpStageValidateEvent) {
      yield await validateSignUpStages(event);
    } else if (event is SignUpStateResetEvent) {
      yield initialState;
    }
  }

  Future<SignUpState> validateSignUpStages(
      SignUpStageValidateEvent event) async {
    switch (event.stage) {
      case 1:
        return validateStage1(event);
        break;
      case 2:
        return validateStage2(event);
        break;
      case 3:
        return validateStage3(event);
        break;
      case 4:
        return validateStage4(event);
        break;
    }

    return null;
  }

  Future<SignUpState> validateStage1(SignUpStageValidateEvent event) async {
    if (firstname.text.trim().isEmpty) {
      return SignUpStageState(
          status: ValidationStatus.formError,
          message: "Firstname is required!",
          stage: event.stage);
    }
    else if (!isAlpha(firstname.text.trim())) {
      return SignUpStageState(
                status: ValidationStatus.formError,
                message: "Firstname cannot contain numbers!",
                stage: event.stage);
    }
     else if (lastname.text.trim().isEmpty) {
      return SignUpStageState(
          status: ValidationStatus.formError,
          message: "Lastname is required!",
          stage: event.stage);
    }
    else if (!isAlpha(lastname.text.trim())) {
      return SignUpStageState(
                status: ValidationStatus.formError,
                message: "Lastname cannot contain numbers!",
                stage: event.stage);
    }
    else {
      return SignUpStageState(
        status: ValidationStatus.success, stage: event.stage);
    }
  }

  Future<SignUpState> validateStage2(SignUpStageValidateEvent event) async {
    if ((email.text.trim().isEmpty) ||
        !DataManipulation.isEmailValid(email.text.trim())) {
      return SignUpStageState(
          status: ValidationStatus.formError,
          message: "Email is required!",
          stage: event.stage);
    }
    return SignUpStageState(
        status: ValidationStatus.success, stage: event.stage);
  }

  Future<SignUpState> validateStage3(SignUpStageValidateEvent event) async {
    if (trn.text.trim().isEmpty) {
      return SignUpStageState(
          status: ValidationStatus.formError,
          message: "TRN is required!",
          stage: event.stage);
    } else if (trn.text.length < 9) {
      return SignUpStageState(
          status: ValidationStatus.formError,
          message: "Please enter a valid TRN!",
          stage: event.stage);
    } else {
      return SignUpStageState(
          status: ValidationStatus.success, stage: event.stage);
    }
  }

  Future<SignUpState> validateStage4(SignUpStageValidateEvent event) async {
    if (pwd.text.trim().isEmpty) {
      return SignUpStageState(
          status: ValidationStatus.formError,
          message: "Password is required!",
          stage: event.stage);
    } else if (pwd.text.trim().length < 8) {
      return SignUpStageState(
          status: ValidationStatus.formError,
          message: "Password must be at least 8 characters",
          stage: event.stage);
    } else if (pwdConfirmation.text.trim().isEmpty) {
      return SignUpStageState(
          status: ValidationStatus.formError,
          message: "Password Confirmation is required!",
          stage: event.stage);
    } else if (pwdConfirmation.text.trim().length < 8) {
      return SignUpStageState(
          status: ValidationStatus.formError,
          message: "Confirmation password must be at least 8 characters.",
          stage: event.stage);
    } else if (pwdConfirmation.text.trim() != pwd.text.trim()) {
      return SignUpStageState(
          status: ValidationStatus.formError,
          message: "Both passwords must be the same",
          stage: event.stage);
    } else {
      return await _createFirebaseUser(event);
    }
  }

  Future<SignUpState> _createFirebaseUser(
      SignUpStageValidateEvent event) async {
    SignUpStatusState signUpStatusState;

    try {
      AuthResult value = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
              email: email.text.trim(), password: pwd.text.trim());

      if (value.user != null) {
        User newUser = new User(
            userId: value.user.uid,
            firstName: this.firstname.text.trim(),
            lastName: this.lastname.text.trim(),
            email: this.email.text.trim(),
            trn: this.trn.text.trim());

        await FirebaseReferences.userAccountsReference
            .child(value.user.uid)
            .set(newUser.toJson())
            .then((value) {
          signUpStatusState = SignUpStatusState(
              status: ValidationStatus.success,
              message: "Account was created and uploaded successfully.",
              user: newUser);
        }).catchError((error) {
          signUpStatusState = SignUpStatusState(
              status: ValidationStatus.success,
              message:
                  "Account was created but the user information wasn't uploaded successfully. Doing a silent retry...",
              user: newUser);
          FirebaseReferences.userAccountsReference
              .child(value.user.uid)
              .set(newUser.toJson());
        });

        return signUpStatusState;
      } else {
        return SignUpStatusState(
            status: ValidationStatus.firebaseError,
            message: "An error occured when creating user account.");
      }
    } on PlatformException catch (exception) {
      signUpStatusState = SignUpStatusState(
          status: ValidationStatus.firebaseError, message: exception.message);
    }

    return signUpStatusState;
  }
}
