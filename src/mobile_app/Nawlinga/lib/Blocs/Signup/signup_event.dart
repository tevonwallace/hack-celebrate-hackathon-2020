
part of 'signup_bloc.dart';

abstract class SignUpEvent extends Equatable {
  const SignUpEvent();
}



class SignUpStateResetEvent extends SignUpEvent {
  const SignUpStateResetEvent();

  @override
  List<Object> get props => [];
}

class SignUpStatusEvent extends SignUpEvent {
  final User user;
  final String password;

  const SignUpStatusEvent({this.user, this.password});

  @override
  List<Object> get props => [this.user, this.password];
}


class SignUpStageValidateEvent extends SignUpEvent {
  final int stage;
  const SignUpStageValidateEvent({this.stage});

  @override
  List<Object> get props => [this.stage];
}
