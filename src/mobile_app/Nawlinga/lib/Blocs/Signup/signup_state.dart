
part of 'signup_bloc.dart';

abstract class SignUpState extends Equatable{
  const SignUpState();
}

class InitialState extends SignUpState{
  @override
  List<Object> get props =>[];
}

enum ValidationStatus {
  success,
  formError,
  firebaseError
}

class SignUpStatusState extends SignUpState{
  final ValidationStatus status;
  final String message;
  final User user;

  const SignUpStatusState({this.status, this.message, this.user});

  @override
  List<Object> get props => [this.status, this.message];
}


class SignUpStageState extends SignUpState{
  final ValidationStatus status;
  final String message;
  final int stage;

  const SignUpStageState({this.status, this.message,this.stage});

  @override
  List<Object> get props => [this.status, this.message];
}

