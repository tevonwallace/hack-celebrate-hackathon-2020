

class DeleteException implements Exception{
  String errMsg() => 'Data was not deleted'; 
}