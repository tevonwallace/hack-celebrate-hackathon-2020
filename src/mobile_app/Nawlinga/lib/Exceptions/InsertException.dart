

class InsertException implements Exception{
  String errMsg() => 'Data was not inserted'; 
}