import 'package:json_annotation/json_annotation.dart';
import 'package:nawlinga/Models/Model.dart';

part 'Reservation.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)

class Reservation extends Model {
  String key;
  String nameOfLocation;
  String photoUrl;
  String date;
  String time;
  
  Reservation({this.key, this.nameOfLocation, this.photoUrl, this.date, this.time});

  factory Reservation.fromJson(Map<dynamic, dynamic> json) => _$ReservationFromJson(json);
  @override
  Map<String, dynamic> toJson() => _$ReservationToJson(this);
}
