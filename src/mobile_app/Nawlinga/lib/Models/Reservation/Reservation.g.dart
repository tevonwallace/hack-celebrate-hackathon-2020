// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Reservation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Reservation _$ReservationFromJson(Map<String, dynamic> json) {
  return Reservation(
    key: json['key'] as String,
    nameOfLocation: json['name_of_location'] as String,
    photoUrl: json['photo_url'] as String,
    date: json['date'] as String,
    time: json['time'] as String,
  );
}

Map<String, dynamic> _$ReservationToJson(Reservation instance) =>
    <String, dynamic>{
      'key': instance.key,
      'name_of_location': instance.nameOfLocation,
      'photo_url': instance.photoUrl,
      'date': instance.date,
      'time': instance.time,
    };
