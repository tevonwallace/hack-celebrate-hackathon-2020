import 'package:json_annotation/json_annotation.dart';
import 'package:nawlinga/Models/Model.dart';
import 'package:nawlinga/Utility/Constants.dart';
import 'package:nawlinga/Utility/DataManipulation.dart';

part 'User.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)

class User extends Model {
  String userId;
  String firstName;
  String lastName;
  String email;
  String trn;
  String date;
  String time;

  User({this.userId, this.firstName, this.lastName, this.email, this.trn}) {
    String dateAndTime = DataManipulation.getCurrentFormattedDateAndTime(format: DateAndTime.format);

    this.date = dateAndTime.split(" ")[0];
    this.time = dateAndTime.split(" ")[1];
  }

  factory User.fromJson(Map<dynamic, dynamic> json) => _$UserFromJson(json);
  @override
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
