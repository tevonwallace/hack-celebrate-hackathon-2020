import 'package:http/http.dart' as http;


///[ApiRequest] is class to make api post request to the server
class ApiRequest {

  ///[baseUrl] is the api host address to which all request will be made to
  static final String baseUrl = "http://api.localhost:3333/";

  ///[postRequest] is a function make a post request to the server without sending any header fields
  Future<http.Response> postRequest(String url,
      {String token, Map<String, dynamic> data}) async {
    Uri finalUrl = Uri.parse(baseUrl + url).replace(queryParameters: data);
    return await http.post(finalUrl, body: data);
  }

  ///[postRequestWithHeaders] is a function make a post request to the server with header fields
  Future<http.Response> postRequestWithHeaders(String url,
      {Map<String, String> headers, Map<String, dynamic> data}) async {
    Uri finalUrl = Uri.parse(baseUrl + url).replace(queryParameters: data);
    return await http.post(finalUrl, body: data, headers: headers);
  }
}
