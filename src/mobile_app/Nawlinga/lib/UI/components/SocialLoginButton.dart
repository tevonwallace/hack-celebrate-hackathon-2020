import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nawlinga/Utility/Constants.dart';

class SocialLoginButton extends StatefulWidget {
  final buttonText;
  final socialIconResource;
  final Color buttonColor;
  final Function onPress;
  final Color splashColor;
  final Color textColor;
  SocialLoginButton(
      {Key key,
      @required this.buttonText,
      @required this.socialIconResource,
      this.buttonColor,
      this.textColor,
      this.splashColor,
      this.onPress})
      : super(key: key);

  @override
  _SocialLoginButtonState createState() => _SocialLoginButtonState();
}

class _SocialLoginButtonState extends State<SocialLoginButton> {
  var buttonText, socialIconResources;

  @override
  void initState() {
    super.initState();
    buttonText = widget.buttonText;
    socialIconResources = widget.socialIconResource;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(color: Colors.black26, blurRadius: 2, offset: Offset(0, 1),spreadRadius: 1),
        ],
      ),
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: FlatButton(
          onPressed: widget.onPress ?? null,
          color: widget.buttonColor ?? Colors.black,
          disabledColor: Colors.transparent,
          highlightColor: Colors.transparent,
          focusColor: Colors.transparent,
          splashColor: widget.splashColor ??Colors.white12,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 13, horizontal: 30),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                    padding:
                        EdgeInsets.only(right: ScreenUtil.screenWidth * .05),
                    child: (socialIconResources is SvgPicture)
                        ? socialIconResources
                        : SvgPicture.asset(
                            socialIconResources,
                            height: 30,
                            color: widget.textColor ?? Colors.white,
                          )),
                Expanded(
                  flex: 1,
                  child: (buttonText is Text)
                      ? buttonText
                      : Text(buttonText,
                          style: NawlingaConstants.getInstance()
                              .appTextStyles
                              .buttonStyle
                              .copyWith(
                                  color: widget.textColor ?? Colors.white,
                                  fontSize: 17)),
                ),
              ],
            ),
          )),
    );
  }
}
