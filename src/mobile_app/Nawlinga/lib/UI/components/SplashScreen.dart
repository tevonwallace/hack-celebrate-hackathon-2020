import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nawlinga/Utility/DataManipulation.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (BuildContext context, Widget widget) {
        return Container(
          color: DataManipulation.getInstance().parseColorFromHex("#ffffff"),
          height: MediaQuery.of(context).size.height,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                    child: Container(
                        height: 100,
                        width: 100,
                        padding: EdgeInsets.only(right: 15),
                        child: SvgPicture.asset(
                          "resources/app_logo/nawlinga_logo.svg",
                          height: 100,
                        ))),
              ]),
        );
      },
    );
  }
}
