import 'package:flutter/material.dart';
import 'package:nawlinga/UI/features/Dashboard/home/home.dart';
import 'package:nawlinga/UI/features/Profile/profile.dart';
import 'package:nawlinga/UI/features/Reservations/reservations.dart';
import 'package:nawlinga/UI/features/Search/search.dart';
import 'package:nawlinga/Utility/DataManipulation.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int _currentIndex = 0;
  List<Widget> titleHeadings = [Text("Dashboard", style: TextStyle(color: DataManipulation.getInstance().parseColorFromHex("#3B3B7E"), fontWeight: FontWeight.bold)),
    Text("Search", style: TextStyle(color: DataManipulation.getInstance().parseColorFromHex("#3B3B7E"), fontWeight: FontWeight.bold)),
    Text("Reservations", style: TextStyle(color: DataManipulation.getInstance().parseColorFromHex("#3B3B7E"), fontWeight: FontWeight.bold)),
    Text("Profile", style: TextStyle(color: DataManipulation.getInstance().parseColorFromHex("#3B3B7E"), fontWeight: FontWeight.bold))];

  List<Widget> _children = [
    Home(),
    SearchView(),
    ReservationsView(),
    ProfileView()
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primarySwatch:
              DataManipulation.getInstance().getMaterialColor("#7076F4"),
          canvasColor:
              DataManipulation.getInstance().getMaterialColor("#7076F4")),
      home: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(40),
          child: AppBar(
            automaticallyImplyLeading: false,
            title: titleHeadings[_currentIndex],
            leading: _currentIndex == 3 ? IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {},
            ): null,
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.add_box),
                onPressed: () {},
              ),
            ],
          ),
        ),
        body: _children[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTabTapped,
          currentIndex: _currentIndex,
          items: [
            new BottomNavigationBarItem(
              icon: Icon(Icons.dashboard),
              title: Text('Dashboard'),
            ),
            new BottomNavigationBarItem(
              icon: Icon(Icons.search),
              title: Text('Search'),
            ),
            new BottomNavigationBarItem(
                icon: Icon(Icons.reorder), title: Text('Reservations')),
            new BottomNavigationBarItem(
                icon: Icon(Icons.person), title: Text('Profile'))
          ],
        ),
      ),
    );
  }
}
