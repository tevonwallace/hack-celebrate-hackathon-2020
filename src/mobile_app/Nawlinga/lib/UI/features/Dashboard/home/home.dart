import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nawlinga/Blocs/Dashboard/Home/home_bloc.dart';
import 'package:nawlinga/Blocs/Signup/signup_bloc.dart';
import 'package:nawlinga/Models/Reservation/Reservation.dart';
import 'package:nawlinga/Models/User/User.dart';
import 'package:nawlinga/Utility/Constants.dart';
import 'package:nawlinga/Utility/DataManipulation.dart';
import 'package:nawlinga/Utility/Firebase/FirebaseServices.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  HomeBloc homeBloc = new HomeBloc();

  User user;
  List<Reservation> reservations;

  FirebaseUser currentUser;

  @override
  void initState() {
    super.initState();

    homeBloc.add(RetrieveHomeUserInfo());
    homeBloc.add(RetrieveHomeUserReservations());

    this._getCurrentUser();
  }

  _getCurrentUser() async {
    currentUser = await FirebaseServices.getInstance().getCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeBloc>.value(
        value: homeBloc,
        child: Scaffold(
            body: SafeArea(
                child: BlocBuilder<HomeBloc, HomeState>(
                    bloc: homeBloc,
                    builder: (BuildContext context, HomeState state) {
                      if (state is UserAccountInformation) {
                        print("Status: " +
                            state.status.toString() +
                            ", Message: " +
                            state.message);

                        if (state.status == ValidationStatus.success) {
                          user = state.user;
                        }
                      } else if (state is UserReservations) {
                        print("User reservations: Status: " +
                            state.status.toString() +
                            ", Message: " +
                            state.message);

                        if (state.status == ValidationStatus.success) {
                          reservations = state.listOfReservations;
                        }
                      }

                      return SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding:
                                  EdgeInsets.only(top: 40, left: 10, right: 10),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "My Profile",
                                style: TextStyle(
                                    fontSize: 30,
                                    color: Colors.white,
                                    fontFamily: "Raleway",
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              child: Container(
                                constraints: BoxConstraints(
                                    maxWidth:
                                        MediaQuery.of(context).size.width),
                                padding: EdgeInsets.only(
                                    top: 25, left: 10, right: 10, bottom: 10),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      child: Container(
                                        alignment: Alignment.centerLeft,
                                        decoration: BoxDecoration(
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.black
                                                    .withOpacity(0.2),
                                                spreadRadius: 0,
                                                blurRadius: 6,
                                                offset: Offset(0,
                                                    3), // changes position of shadow
                                              ),
                                            ],
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  10),
                                                          topRight:
                                                              Radius.circular(
                                                                  10))),
                                              child: Column(
                                                children: <Widget>[
                                                  Row(
                                                    children: <Widget>[
                                                      currentUser == null
                                                          ? Icon(Icons.person,
                                                              size: 60)
                                                          : currentUser
                                                                      .photoUrl ==
                                                                  null
                                                              ? Icon(
                                                                  Icons.person,
                                                                  size: 60)
                                                              : Container(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              10,
                                                                          top:
                                                                              10),
                                                                  child: ClipRRect(
                                                                      borderRadius:
                                                                      BorderRadius.circular(25),
                                                                      child: Image.network(
                                                                          currentUser
                                                                              .photoUrl,
                                                                          loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent loadingProgress) {
                                                                            if (loadingProgress == null) {
                                                                              return child;
                                                                            }
                                                                            else {
                                                                              return Icon(Icons.person, size: 50);
                                                                            }
                                                                          },
                                                                          height:
                                                                              50,
                                                                          fit: BoxFit
                                                                              .fill)
                                                                      ),
                                                                ),
                                                      Text("   "),
                                                      Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                top: 10),
                                                        child: Text(
                                                            user == null
                                                                ? ""
                                                                : (user.firstName +
                                                                    " " +
                                                                    user
                                                                        .lastName),
                                                            style: TextStyle(
                                                                fontSize: 25,
                                                                fontFamily:
                                                                    "Roboto",
                                                                color: Colors
                                                                    .black,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold)),
                                                      ),
                                                    ],
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 15,
                                                        horizontal: 10),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Text(
                                                          reservations == null
                                                              ? "0"
                                                              : reservations
                                                                  .length
                                                                  .toString(),
                                                          style: TextStyle(
                                                              fontSize: 18,
                                                              color:
                                                                  Colors.black,
                                                              fontFamily:
                                                                  "Roboto",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal),
                                                        ),
                                                        Text(
                                                          " Active " +
                                                              (reservations ==
                                                                      null
                                                                  ? "Reservations"
                                                                  : reservations
                                                                              .length ==
                                                                          1
                                                                      ? "Reservation"
                                                                      : "Reservations"),
                                                          style: TextStyle(
                                                              fontSize: 18,
                                                              color:
                                                                  Colors.black,
                                                              fontFamily:
                                                                  "Roboto",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              constraints:
                                                  BoxConstraints(minHeight: 65),
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 5, horizontal: 10),
                                           
                                              decoration: BoxDecoration(
                                                  color: DataManipulation
                                                          .getInstance()
                                                      .parseColorFromHex(
                                                          "#EF844B"),
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  10),
                                                          bottomRight:
                                                              Radius.circular(
                                                                  10))),
                                              child: Container(
                                                  child: (reservations ==
                                                          null)
                                                      ? Container(
                                                        width: MediaQuery.of(context).size.width,
                                                        padding: EdgeInsets.only(top: 20),
                                                        child: Text(
                                                          "No reservations are available",
                                                          maxLines: 2,
                                                          style: TextStyle(
                                                              fontSize: 19,
                                                              color: Colors
                                                                  .white,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              decorationColor:
                                                                  Colors
                                                                      .white)),
                                                      )
                                                      : Container(child: Row(
                                                        mainAxisSize: MainAxisSize.max,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: [
                                                          Column(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: [
                                                            Padding(
                                                              padding: const EdgeInsets.only(top:10),
                                                              child: Text(reservations[0].nameOfLocation,style: TextStyle(
                                                                fontSize:17,
                                                                color:Colors.white,
                                                                fontFamily:"Raleway",
                                                                fontWeight:FontWeight.bold
                                                              ),),
                                                            ),
                                                                Text("@ "+reservations[0].time,
                                                             
                                                                  style: TextStyle(
                                                                  fontSize:16,
                                                                  color:Colors.black,
                                                                  fontFamily:"Raleway",
                                                                  fontWeight:FontWeight.bold
                                                                ),)
                                                          ]),
                                                          Column(children: [
                                                               Text("Up Next",style: TextStyle(
                                                              fontSize:20,
                                                              color:Colors.white,
                                                              fontFamily:"Roboto",
                                                              fontWeight:FontWeight.bold
                                                            ),),
                                                               Padding(
                                                                 padding: const EdgeInsets.only(top:10),
                                                                 child: Text(DataManipulation.getDayFromDate(reservations[0].date)+" "+DataManipulation.getMonthFromDate(reservations[0].date),
                                                             
                                                                    style: TextStyle(
                                                                    fontSize:16,
                                                                    color:Colors.black,
                                                                    fontFamily:"Raleway",
                                                                    fontWeight:FontWeight.bold
                                                                  ),),
                                                               )
                                                          ]
                                                          )
                                                        ],
                                                      ),)
                                            )
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            (reservations == null
                                ? Container()
                                : Container(
                                    padding: EdgeInsets.only(
                                        top: 30, left: 10, right: 10),
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "My Reservations",
                                      style: TextStyle(
                                          fontSize: 25,
                                          fontFamily: "Raleway",
                                          color: Colors.white,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  )),
                            Container(
                              child: Container(
                                constraints: BoxConstraints(
                                    maxWidth: MediaQuery.of(context).size.width,
                                    maxHeight:
                                        MediaQuery.of(context).size.height *
                                            .38),
                                padding: EdgeInsets.only(
                                    top: 10, left: 5, right: 10, bottom: 20),
                                child: ListView.separated(
                                  separatorBuilder: (context, index) => Divider(
                                    color: Colors.black,
                                  ),
                                  scrollDirection: Axis.horizontal,
                                  itemCount: reservations == null
                                      ? 0
                                      : reservations.length,
                                  itemBuilder: (context, index) => Padding(
                                    padding: EdgeInsets.all(10.0),
                                    child: Container(
                                      width: 185.0,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color:
                                                  DataManipulation.getInstance()
                                                      .parseColorFromHex(
                                                          "#4068D8"),
                                              width: 1.5),
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.black.withOpacity(0.2),
                                              blurRadius: 6,
                                              offset: Offset(0,
                                                  3), // changes position of shadow
                                            ),
                                          ],
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20))),
                                      child: Stack(
                                        children: <Widget>[
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: Image.network(
                                                reservations == null
                                                    ? ""
                                                    : reservations[index]
                                                        .photoUrl,
                                                height: 245,
                                                fit: BoxFit.fill),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(top: 207),
                                            child: Container(
                                                height: 50,
                                                width: 185,
                                                padding:
                                                    EdgeInsets.only(left: 10),
                                                decoration: BoxDecoration(
                                                    color:
                                                        DataManipulation.getInstance()
                                                            .parseColorFromHex(
                                                                "#4068D8"),
                                                    border: Border.all(
                                                        color: DataManipulation
                                                                .getInstance()
                                                            .parseColorFromHex(
                                                                "#4068D8"),
                                                        width: 1.5),
                                                    borderRadius: BorderRadius.only(
                                                        topRight:
                                                            Radius.circular(10),
                                                        bottomLeft:
                                                            Radius.circular(10),
                                                        bottomRight:
                                                            Radius.circular(10))),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    Text(
                                                        reservations == null
                                                            ? ""
                                                            : reservations[
                                                                    index]
                                                                .nameOfLocation,
                                                        maxLines: 1,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            fontSize: 18,
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold)),
                                                    Text(
                                                        reservations == null
                                                            ? ""
                                                            : DataManipulation.getFullDate(
                                                                    DateAndTime
                                                                        .dayMonthYear,
                                                                    reservations[
                                                                            index]
                                                                        .date) +
                                                                " @" +
                                                                reservations[
                                                                        index]
                                                                    .time,
                                                        style: TextStyle(
                                                            fontSize: 15,
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold))
                                                  ],
                                                )),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    }))));
  }
}
