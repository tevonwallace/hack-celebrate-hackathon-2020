import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nawlinga/Blocs/Login/login_bloc.dart';
import 'package:nawlinga/Blocs/Signup/signup_bloc.dart';
import 'package:nawlinga/Utility/Constants.dart';
import 'package:nawlinga/routes.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  LoginBloc loginBloc = new LoginBloc();

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 750, height: 1334);
    return BlocProvider<LoginBloc>.value(
      value: loginBloc,
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
          child: BlocListener<LoginBloc, LoginState>(
              bloc: loginBloc,
              listener: (BuildContext context, LoginState state) async {
                if (state is FirebaseAuthenticationForEmailAndPasswordState) {
                  if (state.status == ValidationStatus.success) {
                    Navigator.of(context).pushReplacementNamed(NawlingaRoutes
                        .nawlingaRouteNames[NawlingaRoutePaths.dashboard]);
                  } else {
                    // Three possible status types can be returned. [formError] when the user has an error on the form,
                    // [firebaseError] for firebase errors such as "email already in use" and timeout error and
                    // [success] for when the account was created successfully
                    await AwesomeDialog(
                            context: context,
                            dialogType: DialogType.ERROR,
                            body: Text(state.message),
                            dismissOnTouchOutside: true)
                        .show();

                    loginBloc.add(LoginResetStateEvent());
                  }
                }
              },
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: GestureDetector(
                              onTap: () {
                                NawlingaConstants.getInstance()
                                    .appKeys
                                    .navigationKey
                                    .currentState
                                    .pop();
                              },
                              child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "X",
                                    style: NawlingaConstants.getInstance()
                                        .appTextStyles
                                        .buttonStyle
                                        .copyWith(
                                            fontSize: 30,
                                            fontWeight: FontWeight.w200),
                                  ))),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            "Login",
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .copyWith(fontSize: 20),
                          ),
                        )
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          padding: const EdgeInsets.only(top: 50),
                          child: SvgPicture.asset(
                            "resources/app_logo/nawlinga_logo.svg",
                            fit: BoxFit.contain,
                            width: ScreenUtil().setWidth(400),
                          ),
                        ),
                      ],
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                          padding: EdgeInsets.symmetric(vertical: 30),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                TextField(
                                  textAlign: TextAlign.center,
                                  controller: emailController,
                                  decoration: InputDecoration(
                                      hintText: "Email ",
                                      hintStyle: TextStyle(
                                          fontFamily: "Raleway", fontSize: 17),
                                      enabledBorder: InputBorder.none,
                                      focusedBorder: InputBorder.none),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(vertical: 10),
                                ),
                                TextField(
                                  obscureText: true,
                                  textAlign: TextAlign.center,
                                  controller: passwordController,
                                  decoration: InputDecoration(
                                      hintText: "Password",
                                      hintStyle: TextStyle(
                                          fontFamily: "Raleway", fontSize: 17),
                                      enabledBorder: InputBorder.none,
                                      focusedBorder: InputBorder.none),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black12,
                                          blurRadius: 1,
                                          offset: Offset(0, 1)),
                                    ],
                                  ),
                                  margin: EdgeInsets.only(
                                      left: 15, right: 15, top: 40, bottom: 15),
                                  child: FlatButton(
                                    onPressed: () {
                                      loginBloc.add(
                                          EmailAndPasswordAuthenticate(
                                              email: emailController.text,
                                              password:
                                                  passwordController.text));
                                    },
                                    color: NawlingaConstants.getInstance()
                                        .appColors
                                        .purple,
                                    padding: EdgeInsets.symmetric(vertical: 15),
                                    focusColor: Colors.transparent,
                                    hoverColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    splashColor: Colors.black12,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Center(
                                        child: Text(
                                      "Login",
                                      style: NawlingaConstants.getInstance()
                                          .appTextStyles
                                          .buttonStyle
                                          .copyWith(
                                              fontSize: 20,
                                              color: Colors.white),
                                    )),
                                  ),
                                ),
                                FlatButton(
                                    color: Colors.transparent,
                                    disabledColor: Colors.transparent,
                                    focusColor: Colors.transparent,
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    onPressed: null,
                                    child: Text("Forget Password ?",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1
                                            .copyWith(
                                                fontSize: 16.5,
                                                fontWeight: FontWeight.w400,
                                                decoration:
                                                    TextDecoration.underline))),
                              ])),
                    ),
                  ],
                ),
              )),
        ),
      ),
    );
  }
}
