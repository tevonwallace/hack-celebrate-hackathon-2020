import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nawlinga/Blocs/Signup/signup_bloc.dart';
import 'package:nawlinga/UI/features/signup/signup_stage2.dart';
import 'package:nawlinga/Utility/Constants.dart';

class SignUpStage1 extends StatefulWidget {
  final SignUpBloc signUpBloc;

  SignUpStage1({Key key, this.signUpBloc}) : super(key: key);

  @override
  _SignUpStage1State createState() => _SignUpStage1State();
}

class _SignUpStage1State extends State<SignUpStage1> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 750, height: 1334);
    return BlocProvider<SignUpBloc>.value(
      value: widget.signUpBloc,
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
          child: BlocListener<SignUpBloc, SignUpState>(
              bloc: widget.signUpBloc,
              listener: (BuildContext context, SignUpState state) async {
                if (state is SignUpStageState) {
                  if (state.status == ValidationStatus.formError &&
                      state.message.isNotEmpty) {
                        await AwesomeDialog(
                        context: context,
                            dialogType: DialogType.ERROR,
                        body:Text(state.message),dismissOnTouchOutside: true).show();
                      widget.signUpBloc.add(SignUpStateResetEvent());
                  } else if (state.status == ValidationStatus.success && state.stage==1) {
                    NawlingaConstants.getInstance()
                        .appKeys
                        .navigationKey
                        .currentState
                        .push(
                            MaterialPageRoute(builder: (BuildContext context) {
                      return SignUpStage2(signUpBloc: widget.signUpBloc);
                    }));
                  }
                  widget.signUpBloc.add(SignUpStateResetEvent());
                }
              },
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: GestureDetector(
                              onTap: () {
                                NawlingaConstants.getInstance()
                                    .appKeys
                                    .navigationKey
                                    .currentState
                                    .pop();
                              },
                              child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "X",
                                    style: NawlingaConstants.getInstance()
                                        .appTextStyles
                                        .buttonStyle
                                        .copyWith(
                                            fontSize: 30,
                                            fontWeight: FontWeight.w200),
                                  ))),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            "Signup",
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .copyWith(fontSize: 20),
                          ),
                        )
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          padding: const EdgeInsets.only(top: 50),
                          child: SvgPicture.asset(
                            "resources/app_logo/nawlinga_logo.svg",
                            fit: BoxFit.contain,
                            width: ScreenUtil().setWidth(400),
                          ),
                        ),
                      ],
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                          padding: EdgeInsets.symmetric(vertical: 30),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Container(
                                  width:
                                      ScreenUtil.mediaQueryData.size.width * .8,
                                  child: TextField(
                                    controller: widget.signUpBloc.firstname,
                                    textAlign: TextAlign.center,
                                    decoration: InputDecoration(
                                      hintText: "Firstname",
                                      hintStyle: TextStyle(
                                          fontFamily: "Raleway", fontSize: 17),
                                      enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                              color: Colors.black12)),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                              color: Colors.black12)),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(vertical: 10),
                                ),
                                Container(
                                  width:
                                      ScreenUtil.mediaQueryData.size.width * .8,
                                  child: TextField(
                                    controller: widget.signUpBloc.lastname,
                                    textAlign: TextAlign.center,
                                    decoration: InputDecoration(
                                      hintText: "Lastname",
                                      hintStyle: TextStyle(
                                          fontFamily: "Raleway", fontSize: 17),
                                      enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                              color: Colors.black12)),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                              color: Colors.black12)),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(vertical: 10),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black12,
                                          blurRadius: 1,
                                          offset: Offset(0, 1)),
                                    ],
                                  ),
                                  margin: EdgeInsets.only(
                                      left: 20, right: 20, top: 40, bottom: 15),
                                  child: FlatButton(
                                    onPressed: () {
                                      widget.signUpBloc.add(
                                          SignUpStageValidateEvent(stage: 1));
                                    },
                                    color: NawlingaConstants.getInstance()
                                        .appColors
                                        .purple,
                                    padding: EdgeInsets.symmetric(vertical: 15),
                                    focusColor: Colors.transparent,
                                    hoverColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    splashColor: Colors.black12,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Center(
                                        child: Text(
                                      "Continue",
                                      style: NawlingaConstants.getInstance()
                                          .appTextStyles
                                          .buttonStyle
                                          .copyWith(
                                              fontSize: 20,
                                              color: Colors.white),
                                    )),
                                  ),
                                ),
                              ])),
                    )
                  ],
                ),
              )),
        ),
      ),
    );
  }
}
