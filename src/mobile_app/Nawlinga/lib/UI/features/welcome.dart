import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nawlinga/Blocs/Login/login_bloc.dart';
import 'package:nawlinga/Blocs/Signup/signup_bloc.dart';
import 'package:nawlinga/UI/components/SocialLoginButton.dart';
import 'package:nawlinga/UI/features/signup/signup_stage1.dart';
import 'package:nawlinga/Utility/Constants.dart';
import 'package:nawlinga/Utility/DataManipulation.dart';
import 'package:nawlinga/routes.dart';

class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  LoginBloc loginBloc = new LoginBloc();

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 750, height: 1334);
    return BlocProvider<LoginBloc>.value(
      value: loginBloc,
      child: Scaffold(
        body: SafeArea(
          child: BlocListener<LoginBloc, LoginState>(
              listener: (BuildContext context, LoginState state) {
                if (state is FirebaseAuthenticationState) {
                  if (state.user != null) {
                    NawlingaConstants.getInstance()
                        .appKeys
                        .navigationKey
                        .currentState
                        .pushNamed(NawlingaRoutes
                            .nawlingaRouteNames[NawlingaRoutePaths.dashboard]);
                  } else {
                    // show dialog that authentication fail
                  }
                }
              },
              bloc: loginBloc,
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          padding: const EdgeInsets.only(top: 80, bottom: 3),
                          child: SvgPicture.asset(
                            "resources/app_logo/nawlinga_logo.svg",
                            fit: BoxFit.contain,
                            width: ScreenUtil().setWidth(580),
                          ),
                        ),
                        Text(
                          "Why Wait? When Your Time Matters",
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .copyWith(fontWeight: FontWeight.w200)
                              .copyWith(fontSize: 14),
                        )
                      ],
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 40),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.end,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black26,
                                      blurRadius: 2,
                                      offset: Offset(0, 1),
                                      spreadRadius: 1),
                                ],
                              ),
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              child: FlatButton(
                                onPressed: () {
                                  NawlingaConstants.getInstance()
                                      .appKeys
                                      .navigationKey
                                      .currentState
                                      .pushNamed(
                                          NawlingaRoutes.nawlingaRouteNames[
                                              NawlingaRoutePaths.login]);
                                },
                                color: NawlingaConstants.getInstance()
                                    .appColors
                                    .purple,
                                padding: EdgeInsets.symmetric(vertical: 15),
                                focusColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                splashColor: Colors.black12,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Center(
                                    child: Text(
                                  "Login",
                                  style: NawlingaConstants.getInstance()
                                      .appTextStyles
                                      .buttonStyle
                                      .copyWith(
                                          fontSize: 20, color: Colors.white),
                                )),
                              ),
                            ),
                            Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 18)),
                            Text(
                              "OR",
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .copyWith(fontSize: 20),
                            ),
                            Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 18)),
                            SocialLoginButton(
                              onPress: () {
                                loginBloc.add(AppleAuthenticate(
                                    "username here", "password here"));
                              },
                              socialIconResource:
                                  "resources/icons/apple-logo.svg",
                              buttonText: "Sign In With Apple",
                            ),
                            Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10)),
                            SocialLoginButton(
                                onPress: () {
                                  loginBloc.add(GoogleAuthenticate());
                                },
                                socialIconResource: SvgPicture.asset(
                                  "resources/icons/google-logo.svg",
                                  height: 30,
                                ),
                                buttonText: "Sign In With Google",
                                textColor: Colors.black,
                                splashColor: Colors.black12,
                                buttonColor: Colors.white),
                            Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10)),
                            SocialLoginButton(
                              onPress: () {
                                loginBloc.add(FacebookAuthenticate(
                                    "username here", "password here"));
                              },
                              socialIconResource: SvgPicture.asset(
                                "resources/icons/facebook-logo.svg",
                                height: 30,
                              ),
                              buttonText: "Sign In With facebook",
                              buttonColor: DataManipulation.getInstance()
                                  .parseColorFromHex("#405A94"),
                            ),
                            Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 15)),
                            Align(
                              alignment: Alignment.center,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 15),
                                child: FlatButton(
                                    color: Colors.transparent,
                                    disabledColor: Colors.transparent,
                                    focusColor: Colors.transparent,
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    onPressed: () {
                                      NawlingaConstants.getInstance()
                                          .appKeys
                                          .navigationKey
                                          .currentState
                                          .push(MaterialPageRoute(
                                              builder: (BuildContext context) {
                                          SignUpBloc signUpBloc =new SignUpBloc();
                                        return SignUpStage1(signUpBloc:signUpBloc);
                                      }));
                                    },
                                    child: Text("Sign Up",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1
                                            .copyWith(
                                                fontSize: 22,
                                                decoration:
                                                    TextDecoration.underline))),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              )),
        ),
      ),
    );
  }
}
