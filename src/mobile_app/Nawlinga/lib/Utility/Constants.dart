import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'DataManipulation.dart';


///NawlingaConstants is a class of constant variables that can be use throughout the application

class NawlingaConstants {
  
  static NawlingaConstants _instance;
  _NawlingaAppColors get  appColors => _NawlingaAppColors.getInstance();
  _NawlingaScreenKeys get  appKeys => _NawlingaScreenKeys.getInstance();
  _NawlingaAppTextStyles get  appTextStyles => _NawlingaAppTextStyles.getInstance();

  String get apiAuth => "";

  String get userToken =>"";

  NawlingaConstants._();

  ///A method to return a instance of NawlingaConstants from the memory or instantiate if not
  static NawlingaConstants getInstance() {
    if (_instance == null) _instance = new NawlingaConstants._();
    return _instance;
  }



}

class _NawlingaScreenKeys {
  static _NawlingaScreenKeys _instance;

  _NawlingaScreenKeys._();

  static _NawlingaScreenKeys getInstance() {
    if (_instance == null) _instance = new _NawlingaScreenKeys._();
    return _instance;
  }

  final GlobalKey<NavigatorState> navigationKey = new GlobalKey<NavigatorState>();

}


class _NawlingaAppColors {
  static _NawlingaAppColors _instance;
  _NawlingaAppColors._();

  static _NawlingaAppColors getInstance() {
    if (_instance == null) _instance = _NawlingaAppColors._();
    return _instance;
  }

  Color get purple => DataManipulation.getInstance().parseColorFromHex("#7076F4");


}


class _NawlingaAppTextStyles {
  static _NawlingaAppTextStyles _instance;
  _NawlingaAppTextStyles._();

  static _NawlingaAppTextStyles getInstance() {
    if (_instance == null) _instance = _NawlingaAppTextStyles._();
    return _instance;
  }

  TextStyle get buttonStyle => TextStyle(
    fontSize: 12,
    fontFamily:"Roboto",
    decoration: TextDecoration.none
  );


}

class DateAndTime {
  static final String format = "dd-MM-yyyy hh:mm a";
  static final String dayMonthYear = "d MMM, y";
  static final String formatWithMilliSeconds = "dd-MM-yyyy hh:mm:ss a";
  static final Map<int, String> months = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December"
  };
}
