import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nawlinga/Utility/Constants.dart';

class DataManipulation {
  static DataManipulation _instance;

  DataManipulation._();

  static DataManipulation getInstance() {
    if (_instance == null) {
      _instance = new DataManipulation._();
    }

    return _instance;
  }

  Color parseColorFromHex(String hexValue) {
    // MARK: Used to create a Color from the hex value
    return new Color(int.parse(hexValue.substring(1, 7), radix: 16))
        .withOpacity(1.0);
  }

  MaterialColor getMaterialColor(String hexValue) {
    Map<int, Color> color = {
      50:Color.fromRGBO(136,14,79, .1),
      100:Color.fromRGBO(136,14,79, .2),
      200:Color.fromRGBO(136,14,79, .3),
      300:Color.fromRGBO(136,14,79, .4),
      400:Color.fromRGBO(136,14,79, .5),
      500:Color.fromRGBO(136,14,79, .6),
      600:Color.fromRGBO(136,14,79, .7),
      700:Color.fromRGBO(136,14,79, .8),
      800:Color.fromRGBO(136,14,79, .9),
      900:Color.fromRGBO(136,14,79, 1),
    };

    return MaterialColor(int.parse("0xFF"+hexValue.substring(1)), color);
  }

  static String getCurrentFormattedDateAndTime(
      {bool withNewLine, String format}) {
    String dateAndTime = DataManipulation.getCurrentDateOrTime(format: format);

    return format == null
        ? DataManipulation.getConvertDate("MMMM d, y", dateAndTime.toString()) +
            (withNewLine == null ? " " : "\n") +
            DataManipulation.getConvertDate("h:m a", dateAndTime.toString())
        : dateAndTime;
  }

  static String getCurrentDateOrTime({String format, DateTime date}) {
    return DateFormat(
            format == null ? DateAndTime.formatWithMilliSeconds : format)
        .format(DateTime.now());
  }

  static String getConvertDate(String format, String date) {
    if (date == "" || date == null)
      return null;
    else
      return DateFormat(format).format(DateTime.parse(date));
  }

  static String _changeDateFormat(String date) {
    List<String> list = date.split("-");

    return list[2]+list[1]+list[0];
  }

  static String getDayFromDate(String date) {
    return date.split("-")[0];
  }

  static String getMonthFromDate(String date) {
    return DateAndTime.months[int.parse(date.split("-")[1])];
  }

  static String getFullDate(String format, String date) {
    if (date == "" || date == null)
      return null;
    else
      return DateFormat.yMMMd().format(DateTime.parse(_changeDateFormat(date)));
  }

  static bool isEmailValid(String email) {
    String expression =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

      return new RegExp(expression).hasMatch(email);
  }
}
