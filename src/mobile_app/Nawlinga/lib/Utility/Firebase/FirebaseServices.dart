import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';

class FirebaseKeys {
  static final String userAccounts = "UserAccounts";
  static final String userAccountReservations = "UserReservations";
}

class FirebaseReferences {
  static final DatabaseReference userAccountsReference = FirebaseDatabase.instance.reference().child(FirebaseKeys.userAccounts);
  static final DatabaseReference userAccountReservationsReference = FirebaseDatabase.instance.reference().child(FirebaseKeys.userAccountReservations);
}

class FirebaseServices {
  static FirebaseServices _instance;

  FirebaseServices._();

  static FirebaseServices getInstance() {
    if (_instance == null) {
      _instance = new FirebaseServices._();
    }

    return _instance;
  }

  Future<FirebaseUser> getCurrentUser() async {
    return (await FirebaseAuth.instance.currentUser());
  }
}
