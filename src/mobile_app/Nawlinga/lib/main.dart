import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nawlinga/Utility/Constants.dart';
import 'package:nawlinga/Utility/Firebase/FirebaseServices.dart';
import 'package:nawlinga/routes.dart';

import 'UI/components/SplashScreen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then((_) {
    runApp(NawLinga());
  }).catchError((error) {
    print("Error setting orientation for this device: " + error.toString());

    runApp(NawLinga());
  });
}

class RunApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return NawLinga();
  }
}

class NawLinga extends StatefulWidget {
  @override
  _NawLingaState createState() => _NawLingaState();
}

class _NawLingaState extends State<NawLinga> {
  Future<String> getRouteBasedOnUserLoggedInStatus() async {
    FirebaseUser currentUser = await FirebaseServices.getInstance().getCurrentUser();

    if (currentUser == null) {
      return NawlingaRoutePaths.welcome;
    } else {
      return NawlingaRoutePaths.dashboard;
    }
  }

  String nextRouteName;

  @override
  void initState() {
    super.initState();
    _getRoute();
  }

  _getRoute() async {
    nextRouteName = await getRouteBasedOnUserLoggedInStatus();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return (nextRouteName != null)
        ? MaterialApp(
      navigatorKey: NawlingaConstants.getInstance().appKeys.navigationKey,
      title: 'NawLinga',
      theme: ThemeData(
        textTheme: TextTheme(
          bodyText1: TextStyle(
            fontSize: 12,
            fontFamily: "Roboto"
          )
        ),
        primarySwatch: Colors.blue,
      ),
      routes: NawlingaRoutes.routes,
      initialRoute: NawlingaRoutes.nawlingaRouteNames[nextRouteName],
    )
        : SplashScreen();
  }
}
