import 'package:flutter/material.dart';
import 'package:nawlinga/UI/features/Dashboard/dashboard.dart';
import 'package:nawlinga/UI/features/login/login.dart';
import 'UI/features/welcome.dart';

class NawlingaRoutes {
  static final Map<String, String> nawlingaRouteNames =
      NawlingaRoutePaths._createRoutesMap();

  //Create a Map variable to supply to the Material App routes in the main.dart file route property of the
  static final Map<String, WidgetBuilder> routes = {
    //  Mapping each route address to a widget function / in order words the various screens
    nawlingaRouteNames[NawlingaRoutePaths.welcome]: (BuildContext context) => Welcome(),
    nawlingaRouteNames[NawlingaRoutePaths.login]: (BuildContext context) => Login(),
    nawlingaRouteNames[NawlingaRoutePaths.dashboard]: (BuildContext context) => Dashboard(),
  };
}

class NawlingaRoutePaths {
  static final String welcome = "";
  static final String login = "login";
  static final String dashboard = "dashboard";

  static final List<String> _routeNamesList = [
    NawlingaRoutePaths.welcome,
    NawlingaRoutePaths.login,
    NawlingaRoutePaths.dashboard
  ];

  static Map<String, String> _createRoutesMap() {
    Map<String, String> map = new Map();

    for (int index = 0;
        index < NawlingaRoutePaths._routeNamesList.length;
        index++) {
      String routePath =
          NawlingaRoutePaths._routeNamesList[index] == NawlingaRoutePaths.welcome
              ? "/"
              : "/" +
                  NawlingaRoutePaths._routeNamesList[index][0].toUpperCase() +
                  NawlingaRoutePaths._routeNamesList[index].substring(1);

      map[NawlingaRoutePaths._routeNamesList[index]] = routePath;
    }

    return map;
  }
}
